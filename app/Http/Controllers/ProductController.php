<?php


namespace App\Http\Controllers;



use App\Http\Services\ApiResponseService;
use App\Http\Services\ProductService;
use Illuminate\Http\Request;

class ProductController
{
    private $productService;
    private $apiResponseService;

    public function __construct(
        ProductService $productService,
        ApiResponseService $apiResponseService
    ) {
        $this->productService = $productService;
        $this->apiResponseService = $apiResponseService;
    }

    public function index()
    {
        return $this->apiResponseService->successResponse($this->productService->fetchProducts());
    }

    public function show($product)
    {
        return $this->apiResponseService->successResponse($this->productService->fetchProduct($product));
    }

    public function store(Request $request)
    {
        return $this->apiResponseService->successResponse($this->productService->createProduct($request->all()));
    }

    public function update(Request $request, $product)
    {
        return $this->apiResponseService->successResponse($this->productService->updateProduct($product, $request->all()));
    }

    public function destroy($product)
    {
        return $this->apiResponseService->successResponse($this->productService->deleteProduct($product));
    }
}