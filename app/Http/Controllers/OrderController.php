<?php


namespace App\Http\Controllers;


use App\Http\Services\ApiResponseService;
use App\Http\Services\OrderService;
use App\Http\Services\ProductService;
use Illuminate\Http\Request;

class OrderController
{
    private $orderService;

    private $productService;

    private $apiResponseService;

    public function __construct(
        OrderService $orderService,
        ProductService $productService,
        ApiResponseService $apiResponseService
    ) {
        $this->orderService = $orderService;
        $this->productService = $productService;
        $this->apiResponseService = $apiResponseService;
    }

    public function index()
    {
        return $this->apiResponseService->successResponse($this->orderService->fetchOrders());
    }

    public function show($order)
    {
        return $this->apiResponseService->successResponse($this->orderService->fetchOrder($order));
    }

    public function store(Request $request)
    {
        $result = $this->productService->fetchProduct($request->product_id);

        return $this->apiResponseService->successResponse($this->orderService->createOrder($request->all()));
    }

    public function update(Request $request, $order)
    {
        return $this->apiResponseService->successResponse($this->orderService->updateOrder($order, $request->all()));
    }

    public function destroy($order)
    {
        return $this->apiResponseService->successResponse($this->orderService->deleteOrder($order));
    }
}