<?php


namespace App\Http\Services;

class ProductService
{
    private RequestService $requestService;

    public $baseUri;

    public $secret;

    public function __construct(RequestService $requestServices)
    {
        $this->baseUri = config('services.products.base_uri');
        $this->secret = config('services.products.secret');
        $this->requestService = $requestServices;
    }
    public function fetchProducts()
    {
        return $this->requestService->request('GET', '/api/product', $this->baseUri, $this->secret);
    }
    public function fetchProduct($product)
    {
        return $this->requestService->request('GET', "/api/product/{$product}", $this->baseUri, $this->secret);
    }
    public function createProduct($data)
    {
        return $this->requestService->request('POST', '/api/product', $this->baseUri, $this->secret, $data);
    }
    public function updateProduct($product, $data)
    {
        return $this->requestService->request('POST', "/api/product/{$product}", $this->baseUri, $this->secret, $data);
    }
    public function deleteProduct($product)
    {
        return $this->requestService->request('GET', "/api/product/delete/{$product}", $this->baseUri, $this->secret);
    }
}