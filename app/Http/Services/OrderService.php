<?php


namespace App\Http\Services;


class OrderService
{
    private $request;

    public $baseUri;

    public $secret;

    public function __construct(RequestService $request)
    {
        $this->baseUri = config('services.orders.base_uri');
        $this->secret = config('services.orders.secret');
        $this->request = $request;
    }
    public function fetchOrders()
    {
        return $this->request->request('GET', '/api/order', $this->baseUri, $this->secret);
    }
    public function fetchOrder($order)
    {
        return $this->request->request('GET', "/api/order/{$order}", $this->baseUri, $this->secret);
    }
    public function createOrder($data)
    {
        return $this->request->request('POST', '/api/order', $this->baseUri, $this->secret, $data);
    }
    public function updateOrder($order, $data)
    {
        return $this->request->request('POST', "/api/order/{$order}", $this->baseUri, $this->secret, $data);
    }
    public function deleteOrder($order)
    {
        return $this->request->request('GET', "/api/order/delete/{$order}", $this->baseUri, $this->secret);
    }
}