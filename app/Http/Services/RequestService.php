<?php


namespace App\Http\Services;

use GuzzleHttp\Client;

class RequestService
{
    public function request($method, $requestUrl, $baseUri, $secret, $formParams = [], $headers = [])
    {
        $client = new Client([
            'base_uri' => $baseUri
        ]);
        if (isset($secret)) {
            $headers['Authorization'] = $secret;
        }
        $response = $client->request($method, $requestUrl,
            [
                'form_params' => $formParams,
                'headers' => $headers
            ]
        );
        return $response->getBody()->getContents();
    }
}